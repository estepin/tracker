<?php

namespace Tracker\slugs;

use Tracker\interfaces\Slug;
use Tracker\ProxyParamsDto;
use Tracker\TrackStatus;

class FedexAftership implements Slug
{
    protected $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36';
    protected $proxy;

    function getStatusByTrackNumber($trackNumber)
    {
        $content = $this->request($trackNumber);
        if(isset($content['data']['direct_trackings'][0]['tracking']['latest_status'])) {
            $textStatus = $content['data']['direct_trackings'][0]['tracking']['latest_status'];

            $trackStatus = new TrackStatus();
            $trackStatus->trackStatusText = $textStatus;
            $trackStatus->slugLabel = "Fedex";

            if($textStatus == "In Transit") {
                $trackStatus->trackStatus = TrackStatus::IN_TRANSIT;
            }elseif($textStatus == 'Delivered') {
                $trackStatus->trackStatus = TrackStatus::DELIVERED;
            } elseif($textStatus == 'Not found') {
                $trackStatus->trackStatus = TrackStatus::NOT_FOUND;
            }

            return $trackStatus;
        }

        throw new \Exception('Unknown response');
    }

    function setProxy(ProxyParamsDto $proxy)
    {
        $this->proxy = $proxy;
    }

    protected function request($track)
    {
        $rData = [
            'direct_trackings' => [
                ['tracking_number' => $track, 'additional_fields' => new \ArrayObject(), 'slug' => 'fedex']
            ]
        ];

        $ch = curl_init('https://track.aftership.com/api/v2/direct-trackings/batch');
        curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($rData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Accept: application/json',
            'Connection: Close'
        ]);


        if($this->proxy) {
            if(!$this->proxy->port || !$this->proxy->ip) {
                throw new \Exception("Invalid proxy");
            }
            curl_setopt( $ch, CURLOPT_PROXY, $this->proxy->ip);
            curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxy->port);
            if($this->proxy->password) {
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

                if($this->proxy->login && $this->proxy->password) {
                    curl_setopt( $ch, CURLOPT_PROXYUSERPWD, $this->proxy->login.':'.$this->proxy->password);
                } else {
                    curl_setopt( $ch, CURLOPT_PROXYUSERPWD, $this->proxy->password);
                }

            }
        }

        $content = curl_exec($ch);
        return json_decode($content, true);
    }
}