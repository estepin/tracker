<?php
namespace Tracker\slugs;

use Tracker\interfaces\Slug;
use Tracker\ProxyParamsDto;
use Tracker\TrackStatus;


class Fedex implements Slug
{
    protected $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36';
    protected $proxy;

    function getStatusByTrackNumber($trackNumber)
    {
        $statusDTO = new TrackStatus();
        $bearer = $this->getAuthBearer();
        $status = $this->request($trackNumber, $bearer);

        if($status == 'In-Transit') {
            $statusDTO->trackStatus = TrackStatus::IN_TRANSIT;
        }elseif($status == 'Delivered') {
            $statusDTO->trackStatus = TrackStatus::DELIVERED;
        }elseif ($status == 'Not found') {
            $statusDTO->trackStatus = TrackStatus::NOT_FOUND;
        }
        $statusDTO->trackStatusText = $status;
        $statusDTO->slugLabel = "Fedex";

        return $statusDTO;
    }

    function setProxy(ProxyParamsDto $proxy)
    {
        $this->proxy = $proxy;
    }

    protected function getConf()
    {
        $content = file_get_contents('https://www.fedex.com/wtrk/trackingmodule_hp/HomepageWebComponent/properties/HomepageProperties.json');
        $data = json_decode($content, true);
        if(isset($data['api']['client_id']) && isset($data['api']['client_secret'])) {
            return [
                'client_id' => $data['api']['client_id'],
                'client_secret' => $data['api']['client_secret'],
            ];
        } else {
            throw new \Exception('Unknown response');
        }
    }

    protected function getAuthBearer()
    {
        $conf = $this->getConf();

        $url = sprintf(
            'https://api.fedex.com/auth/oauth/v2/token?grant_type=%s&client_id=%s&client_secret=%s',
            'client_credentials',
            $conf['client_id'],
            $conf['client_secret']
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        $content = curl_exec($ch);

        $data = json_decode($content, true);
        if(!isset($data['access_token'])) {
            throw new \Exception('Unknown response');
        }
        return $data['access_token'];
    }

    protected function request($trackNumber, $bearer)
    {
        $rData = [
            'appType' => 'WTRK',
            'appDeviceType' => 'WTRK',
            'supportCurrentLocation' => true,
            'trackingInfo' => [
                [
                    'trackNumberInfo' => [
                        'trackingNumber' => $trackNumber,
                        'trackingQualifier' => '',
                        'trackingCarrier' => ''
                    ]
                ]
            ],
            'uniqueKey' => ''
        ];

        $ch = curl_init("https://api.fedex.com/track/v2/shipments");
        curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_ENCODING, '' );

        if($this->proxy) {
            if(!$this->proxy->port || !$this->proxy->ip) {
                throw new \Exception("Invalid proxy");
            }
            curl_setopt( $ch, CURLOPT_PROXY, $this->proxy->ip);
            curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxy->port);
            if($this->proxy->password) {
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

                if($this->proxy->login && $this->proxy->password) {
                    curl_setopt( $ch, CURLOPT_PROXYUSERPWD, $this->proxy->login.':'.$this->proxy->password);
                } else {
                    curl_setopt( $ch, CURLOPT_PROXYUSERPWD, $this->proxy->password);
                }

            }
        }


        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            sprintf('Authorization: Bearer %s', $bearer),
            'Content-Type: application/json',
            'Accept-Encoding: gzip, deflate',
            'Connection: Close'
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($rData));

        $content = curl_exec($ch);

        $resultData = json_decode($content, true);
        if(isset($resultData['output']['packages'][0]['keyStatus']) && $status = $resultData['output']['packages'][0]['keyStatus']) {
            return $status;
        }

        throw new \Exception('Unknown response');
    }
}