<?php
namespace Tracker\slugs;

use Tracker\interfaces\Slug;
use Tracker\ProxyParamsDto;
use Tracker\TrackStatus;

class Ups implements Slug
{
    protected $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36';
    protected $keys = false;
    protected $proxy;

    function getStatusByTrackNumber($trackNumber)
    {
        $textStatus = $this->request([
            'Locale' => 'en_CA',
            'TrackingNumber' => [$trackNumber],
            'Requester' => 'wt/trackdetails'
        ]);

        $trackStatus = new TrackStatus();
        $trackStatus->trackStatusText = $textStatus;
        $trackStatus->slugLabel = "UPS";

        if($textStatus == "In Transit") {
            $trackStatus->trackStatus = TrackStatus::IN_TRANSIT;
        }elseif($textStatus == 'Delivered') {
            $trackStatus->trackStatus = TrackStatus::DELIVERED;
        } elseif($textStatus == 'Not found') {
            $trackStatus->trackStatus = TrackStatus::NOT_FOUND;
        }

        return $trackStatus;
    }

    function setProxy(ProxyParamsDto $proxy)
    {
        $this->proxy = $proxy;
    }

    protected function request($data)
    {
        $try = 2;
        while($try) {
            $try--;
            $keys = $this->getKeys();

            $ch = curl_init("https://www.ups.com/track/api/Track/GetStatus?loc=en_CA");
            curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type:application/json',
                'Accept:application/json',
                'X-XSRF-TOKEN:'.$keys['X-XSRF-TOKEN']
            ]);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

            $cookieStr = 'X-CSRF-TOKEN='.$keys['cookies']['X-CSRF-TOKEN'].';';
            $cookieStr .= 'X-XSRF-TOKEN-ST='.$keys['cookies']['X-XSRF-TOKEN-ST'].';';
            curl_setopt($ch, CURLOPT_COOKIE, $cookieStr);

            $res = curl_exec($ch);

            $resultData = json_decode($res, true);

            if(!isset($resultData['unauthorized'])) {
                break;
            } else {
                $this->keys = false;
            }
        }

        if(isset($resultData['unauthorized'])) {
            throw new \Exception('Unauthorized');
        }

        if(!isset($resultData['statusCode']) || !isset($resultData['trackDetails'][0]['packageStatus'])) {
            throw new \Exception('Unknown response');
        } else {
            if($resultData['trackDetails'][0]['errorCode'] == 504) {
                return 'Not found';
            } elseif ($resultData['trackDetails'][0]['packageStatus']) {
                return $resultData['trackDetails'][0]['packageStatus'];
            }
        }

        throw new \Exception('Unknown response');
    }

    protected function getKeys()
    {
        if(!$this->keys) {
            $ch = curl_init('https://www.ups.com/track?loc=en_US&requester=ST/');
            curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 1);

            $res = curl_exec($ch);

            preg_match_all('|Set-Cookie: (.*?);|is', $res, $matches);
            $cookies = $matches[1];
            foreach ($cookies as $cookieStr) {
                $ex = explode('=', $cookieStr);
                $cookiesResult[$ex[0]] = $ex[1];
            }

            $this->keys = [
                'X-XSRF-TOKEN' => $cookiesResult['X-XSRF-TOKEN-ST'],
                'cookies' => $cookiesResult
            ];
        }

        return $this->keys;
    }

}