<?php
namespace Tracker\slugs;

use Tracker\interfaces\Slug;
use Tracker\ProxyParamsDto;
use Tracker\TrackStatus;

class Usps implements Slug
{
    protected $key;
    protected $proxy;

    function __construct($key)
    {
        $this->key = $key;
    }

    function setProxy(ProxyParamsDto $proxy)
    {
        $this->proxy = $proxy;
    }

    function getStatusByTrackNumber($trackNumber)
    {
        $statusDTO = new TrackStatus();
        $content = $this->request($trackNumber, 'w3IsGuY1='.$this->key.';');
        $status = $this->parse($content);

        $deliveredStatus = [
            'Delivered, In/At Mailbox',
            'Delivered'
        ];

        $inTransitStatus = [
            'In-Transit'
        ];

        if(in_array($status, $inTransitStatus)) {
            $statusDTO->trackStatus = TrackStatus::IN_TRANSIT;
        }elseif(in_array($status, $deliveredStatus) || preg_match('|delivered|is', $status)) {
            $statusDTO->trackStatus = TrackStatus::DELIVERED;
        }
        $statusDTO->trackStatusText = $status;
        $statusDTO->slugLabel = "USPS";

        return $statusDTO;
    }

    protected function request($trackNumber, $cookie)
    {
        $agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36';

        $ch = curl_init("https://tools.usps.com/go/TrackConfirmAction?qtc_tLabels1=".$trackNumber);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $content = curl_exec($ch);

        return $content;
    }

    protected function parse($content)
    {
        $matches = [];
        preg_match('!<div class="delivery_status">.*?<h2>.*?<strong>(.*?)</strong>!is', $content, $matches);

        if(isset($matches[1])) {
            return $matches[1];
        } else {
            throw new \Exception("Can\'t parse content");
        }
    }
}