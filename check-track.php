<?php
require_once "vendor/autoload.php";

use Tracker\Tracker;
if(!isset($argv[1])) {
    echo "Need track".PHP_EOL;
    die;
}
$track = $argv[1];

$trackService = new Tracker(
    [
        Tracker::SLUG_FEDEX => new \Tracker\slugs\Fedex(),
        Tracker::SLUG_UPS => new \Tracker\slugs\Ups(),
        Tracker::SLUG_USPS => new \Tracker\slugs\Usps()
    ]
);

printf("Try get status by %s\n", $track);
try{
    $status = $trackService->getStatus($track);
} catch (\Tracker\exceptions\SlugNotDetected $exception) {
    printf("%s %s\n\n", $track, "Can't slug detected");
    die;
} catch (\Exception $e) {
    echo "Some was wrong\n";
    die;
}

printf("%s %s %s\n\n", $track, $status->slugLabel, $status->trackStatusText);