<?php
namespace Tracker\interfaces;

use Tracker\TrackModel;

interface TrackRepository
{
    function save(TrackModel $model);
    function getTrackByTrackNumber($trackNumber);
}