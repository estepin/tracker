<?php
namespace Tracker\interfaces;


interface SlugDetector
{
    function detectByTrack($track);
}