<?php
namespace Tracker\interfaces;

use Tracker\ProxyParamsDto;

interface Slug
{
    function getStatusByTrackNumber($trackNumber);

    function setProxy(ProxyParamsDto $proxy);
}