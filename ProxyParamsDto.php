<?php

namespace Tracker;

class ProxyParamsDto
{
    function __construct($ip, $port, $login = null, $password = null)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->login = $login;
        $this->password = $password;
    }

    public $ip;
    public $port;
    public $login;
    public $password;
}