<?php
namespace Tracker;

class TrackStatus
{
    const IN_TRANSIT = 1;
    const DELIVERED = 2;
    const NOT_FOUND = 3;

    protected $_trackStatus;
    protected $_trackStatusText;
    protected $_slugLabel;

    function __construct()
    {
        $this->_trackStatus = false;
        $this->_trackStatusText = false;
        $this->_slugLabel = false;
    }

    function __set($origName, $value)
    {
        $acceptableStatuses = [self::IN_TRANSIT, self::DELIVERED, self::NOT_FOUND];

        if($origName == 'trackStatus' && array_search($value, $acceptableStatuses) === false) {
            throw new \Exception('Unknown status '.$value);
        }

        $name = '_'.$origName;
        if(isset($this->$name)) {
            $this->$name = $value;
        } else {
            throw new \ErrorException("Unknown property ".$origName);
        }

    }

    function __get($name)
    {
        $name = '_'.$name;
        return $this->$name;
    }
}