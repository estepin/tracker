<?php
namespace Tracker;


class SlugDetector implements \Tracker\interfaces\SlugDetector
{
    function detectByTrack($track)
    {
        if(strlen($track) >= 22 && substr($track, 0, 1) == '9')
        {
            return Tracker::SLUG_USPS;
        }
        elseif (strlen($track) >= 12 && strlen($track) <= 20 && preg_match("|^[0-9]*$|is", $track))
        {
            return Tracker::SLUG_FEDEX;
        }
        elseif (strlen($track) == 18 && preg_match('|^1Z*|is', $track))
        {
            return Tracker::SLUG_UPS;
        }

        return false;
    }
}