<?php
namespace Tracker;

use Tracker\exceptions\SlugNotDetected;
use Tracker\interfaces\Slug;

class Tracker
{
    const SLUG_FEDEX = 1;
    const SLUG_USPS = 4;
    const SLUG_UPS = 2;

    protected $slugServices;
    protected $slugDetector;
    protected $proxies = [];

    private $lastProxyIndex = 0;

    function __construct($slugServices = false, \Tracker\interfaces\SlugDetector $slugDetector = null)
    {
        if($slugServices) {
            foreach ($slugServices as $type => $slug) {
                $this->addSlugService($slug, $type);
            }
        }

        if($slugDetector) {
            $this->slugDetector = $slugDetector;
        } else {
            $this->slugDetector = new SlugDetector();
        }

    }

    function addSlugService(Slug $slug, $slugType)
    {
        $acceptableTypes = [self::SLUG_FEDEX, self::SLUG_UPS, self::SLUG_USPS];
        if(array_search($slugType, $acceptableTypes) === false) {
            throw new \Exception('Unknown slug type');
        }

        $this->slugServices[$slugType] = $slug;
    }

    function addToServe($trackNumber, $slug = false)
    {

    }

    function getStatus($trackNumber)
    {
        $slugType = $this->detectSlug($trackNumber);
        if(!$slugType) {
            throw new SlugNotDetected("Can't slug detected");
        }
        $status = $this->getSlug($slugType)->getStatusByTrackNumber($trackNumber);
        return $status;
    }

    function addProxy(ProxyParamsDto $proxy)
    {
        $this->proxies[] = $proxy;
    }

    function applyProxy(Slug $slug)
    {
        $proxy = $this->getActualProxy();
        if($proxy) {
            $slug->setProxy($proxy);
        }
    }

    protected function getActualProxy()
    {
        if(count($this->proxies)) {
            if($this->lastProxyIndex >= (count($this->proxies) - 1))
            {
                $this->lastProxyIndex = 0;
            } else {
                $this->lastProxyIndex++;
            }

            return $this->proxies[$this->lastProxyIndex];
        } else {
            return false;
        }
    }


    protected function detectSlug($trackNumber)
    {
        return $this->slugDetector->detectByTrack($trackNumber);
    }

    protected function getSlug($type)
    {
        $slug = $this->slugServices[$type];
        $this->applyProxy($slug);
        return $slug;
    }
}